﻿namespace CleanDuplicateFusionFiles
{
    partial class frmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOpenAndAnalyseFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.btnClean = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnOpenAndAnalyseFolder
            // 
            this.btnOpenAndAnalyseFolder.Location = new System.Drawing.Point(12, 12);
            this.btnOpenAndAnalyseFolder.Name = "btnOpenAndAnalyseFolder";
            this.btnOpenAndAnalyseFolder.Size = new System.Drawing.Size(300, 45);
            this.btnOpenAndAnalyseFolder.TabIndex = 0;
            this.btnOpenAndAnalyseFolder.Text = "Open And Analyse Folder";
            this.btnOpenAndAnalyseFolder.UseVisualStyleBackColor = true;
            this.btnOpenAndAnalyseFolder.Click += new System.EventHandler(this.btnOpenAndAnalyseFolder_Click);
            // 
            // rtbOutput
            // 
            this.rtbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbOutput.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.rtbOutput.Location = new System.Drawing.Point(12, 77);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.Size = new System.Drawing.Size(1454, 855);
            this.rtbOutput.TabIndex = 3;
            this.rtbOutput.Text = "";
            this.rtbOutput.WordWrap = false;
            // 
            // btnClean
            // 
            this.btnClean.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClean.Enabled = false;
            this.btnClean.Location = new System.Drawing.Point(1166, 12);
            this.btnClean.Name = "btnClean";
            this.btnClean.Size = new System.Drawing.Size(300, 45);
            this.btnClean.TabIndex = 4;
            this.btnClean.Text = "Delete Duplicate Files";
            this.btnClean.UseVisualStyleBackColor = true;
            this.btnClean.Click += new System.EventHandler(this.btnClean_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1478, 944);
            this.Controls.Add(this.btnClean);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.btnOpenAndAnalyseFolder);
            this.Name = "frmMain";
            this.Text = "Delete Duplicate Fusion Files";
            this.ResumeLayout(false);

        }

        #endregion

        private Button btnOpenAndAnalyseFolder;
        private FolderBrowserDialog folderBrowserDialog1;
        private RichTextBox rtbOutput;
        private Button btnClean;
    }
}