﻿using System;

public class PathWithHashContainer
{
    public string path;
    public string hash;

    public PathWithHashContainer(string path, string hash)
    {
        this.path = path;
        this.hash = hash;
    }
}