using CleanDuplicateFusionFiles.Properties;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Windows.Forms;
using System.IO;

namespace CleanDuplicateFusionFiles
{ 
    public partial class frmMain : Form
    {
        List<PathWithHashContainer> allPathWithHashContainers = new List<PathWithHashContainer>();
        List<PathWithHashContainer> tempPathWithHashContainers = new List<PathWithHashContainer>();
        List<PathWithHashContainer> uniquePathWithHashContainers = new List<PathWithHashContainer>();
        List<PathWithHashContainer> deletePathWithHashContainers = new List<PathWithHashContainer>();

        public string lineBuffer = "";

        public void AddLineToBuffer(string line)
        {
            lineBuffer += line + "\n";
        }

        public void ShowBuffer()
        {
            rtbOutput.Text = lineBuffer;
            rtbOutput.SelectionStart = rtbOutput.Text.Length;
            rtbOutput.ScrollToCaret();
        }

        public frmMain()
        {
            InitializeComponent(); 

            // Preselect the last folder we worked in
            folderBrowserDialog1.SelectedPath = Settings.Default.LastPath;
        }

        private void AnalyseFolder()
        {
            lineBuffer = "";
            allPathWithHashContainers.Clear();
            tempPathWithHashContainers.Clear();
            uniquePathWithHashContainers.Clear();
            deletePathWithHashContainers.Clear();

            MD5 md5 = MD5.Create();

            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                AddLineToBuffer("Analysing all files in '" + folderBrowserDialog1.SelectedPath + "'");
                AddLineToBuffer("-------------------------------------");

                // Save selected path for next program start
                Settings.Default.LastPath = folderBrowserDialog1.SelectedPath;
                Settings.Default.Save();

                // Iterate over all files in folder, calculate their hashes and generate the PathWithHashContainers
                foreach (String fileName in Directory.GetFiles(folderBrowserDialog1.SelectedPath))
                {
                    byte[] hash = md5.ComputeHash(File.ReadAllBytes(fileName));
                    string hashAsString = "";

                    foreach (byte h in hash)
                        hashAsString += h.ToString("x2");

                    allPathWithHashContainers.Add(new PathWithHashContainer(fileName, hashAsString));

                    AddLineToBuffer("Found: " + hashAsString + " " + fileName);
                }
            }
            else
            {
                // If dialoge did not return a path, there is not much to do
                return;
            }

            AddLineToBuffer("-------------------------------------");
            AddLineToBuffer("Analyse found " + allPathWithHashContainers.Count + " files");

            AddLineToBuffer("");

            AddLineToBuffer("Generate list of files with unique hashes");
            AddLineToBuffer("-------------------------------------");

            // Important step, order list alphabetically to retain file with lowest number in name
            allPathWithHashContainers = allPathWithHashContainers.OrderBy(h => h.path).ToList();

            // Add the complete list of all PathWithHash containers to our temp list
            tempPathWithHashContainers.AddRange(allPathWithHashContainers);

            // While the temp PathWithHashContainer list contains elements
            // -> Take the first element from the list and add it to the list of unique PathWithHashContainers
            // -> Remove all occurances from this hash from the temp PathWithHashContainers list
            while (tempPathWithHashContainers.Count > 0)
            {
                PathWithHashContainer pathWithHash = tempPathWithHashContainers[0];
                uniquePathWithHashContainers.Add(pathWithHash);
                tempPathWithHashContainers.RemoveAll(h => h.hash == pathWithHash.hash);

                AddLineToBuffer("Will Keep: " + pathWithHash.hash + " " + pathWithHash.path);
            }

            AddLineToBuffer("-------------------------------------");
            AddLineToBuffer("Found " + uniquePathWithHashContainers.Count + " files with unique hashes");

            AddLineToBuffer("");

            AddLineToBuffer("Generate list of files to delete");
            AddLineToBuffer("-------------------------------------");

            // Add all containers to the delete container list
            deletePathWithHashContainers.AddRange(allPathWithHashContainers);

            // Remove containers we want to keep from delete list 
            foreach (PathWithHashContainer pathWithHash in uniquePathWithHashContainers)
                deletePathWithHashContainers.Remove(pathWithHash);

            // Create output for user to show which file is going to be deleted 
            foreach (PathWithHashContainer pathWithHash in deletePathWithHashContainers)
                AddLineToBuffer("Will Delete: " + pathWithHash.hash + " " + pathWithHash.path);

            AddLineToBuffer("-------------------------------------");
            AddLineToBuffer("Found " + deletePathWithHashContainers.Count + " files to be deleted");

            AddLineToBuffer("");

            // Write buffer to textbox
            ShowBuffer();

            // If any, allow deletion of duplicate files
            if (deletePathWithHashContainers.Count > 0)
                btnClean.Enabled = true;
        }

        private void btnClean_Click(object sender, EventArgs e)
        {
            AddLineToBuffer("Starting the process of deleting files");
            AddLineToBuffer("-------------------------------------");

            int deletePathWithHashContainersCount = deletePathWithHashContainers.Count;

            foreach (PathWithHashContainer pathWithHash in deletePathWithHashContainers)
            {
                AddLineToBuffer("Did Delete: " + pathWithHash.hash + " " + pathWithHash.path);
                File.Delete(pathWithHash.path);
            }

            AddLineToBuffer("-------------------------------------");
            AddLineToBuffer("Finished deleting " + deletePathWithHashContainersCount + " files");

            ShowBuffer();
            btnClean.Enabled = false;
        }

        private void btnOpenAndAnalyseFolder_Click(object sender, EventArgs e)
        {
            AnalyseFolder();
        }
    }
}