# Clean Duplicate Fusion Files
 
This project was created out of a niche need of mine. At the time of writing, Fusion 360 had for over a year an odd component duplication bug which affected many of my CAD projects. Since I had to clean up my exports each time per hand, I decided to develop this tool.
 
# What Does This Tool Do
 
This tool allows you to analyze folders for files with the same content, but different filenames. Also it gives a comprehensive listing of all of its actions, with a seperate button to delete all the duplicated files.
 
# How can I use this tool
 
Either you build the tool from source (Clone the project and open it with Visual Studio) or you can use the exe that comes with it.
 
# Here are some pictures showing the workflow with this tool
 
## Export the assembly in fusion
![Minion](Images/Step0.JPG)
 
## Select one file per body
![Minion](Images/Step1.JPG)
 
## See the duplication bug in action
![Minion](Images/Step2.JPG)
 
## Open the CleanDuplicateFusionFiles tool
![Minion](Images/Step3.JPG)
 
## Select the export folder
![Minion](Images/Step4.JPG)
 
## Click on Delete Duplicate Files
![Minion](Images/Step5.JPG)
 
## See how the folder now only contains one copy of each component
![Minion](Images/Step6.JPG)

